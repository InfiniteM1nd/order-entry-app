import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, IonicPageModule, NavParams } from 'ionic-angular';


import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TrackPage } from '../pages/tracking/track';
import { OrderPage } from '../pages/order/order';
import { LoginPage } from '../pages/login/login';
import { AddressBookPage } from '../pages/addressBook/address.book';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatFormFieldModule, MatInputModule, MatOptionModule, MatSelectModule} from '@angular/material';

import { HelpPage } from '../pages/help/help';

import { Firebase } from '@ionic-native/firebase';
import {HttpModule} from '@angular/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { LoginModule } from '../pages/login/login.module';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';
import { HttpClientModule } from '@angular/common/http';

import { AuthService } from './shared/service/auth.service';
// import { Contacts, Contact, ContactName } from '@ionic-native/contacts';
import { ApiService } from './shared/service/api.service';
import { EditAddressBookPage } from '../pages/addressBook/edit-address';
import { AddAddressBookPage } from '../pages/addressBook/add-address';

import { SearchPipe } from '../pipes/search/search';
import { DisplayObjectValuePipe } from '../pipes/keyspipe/objectvalues';
import { TrackingModalPage } from '../pages/tracking/tracking-details-modal';

        // Initialize Firebase
        export const firebaseConfig = {
          production: false,
          firebase: {
            apiKey: "AIzaSyDCqNcRRXLbC5mn1zyT0lVtnwvnwWOcooU",
            authDomain: "orders-ab797.firebaseapp.com",
            databaseURL: "https://orders-ab797.firebaseio.com",
            projectId: "orders-ab797",
            storageBucket: "orders-ab797.appspot.com",
            messagingSenderId: "172442676194"
          }
      };

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TrackPage,
    OrderPage,
    AddressBookPage,
    HelpPage,
    EditAddressBookPage,
    AddAddressBookPage,
    TrackingModalPage,
    SearchPipe,
    DisplayObjectValuePipe
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatOptionModule,
    MatSelectModule,
    ReactiveFormsModule,
    HttpModule,
    FormsModule,
    FormsModule,
    LoginModule,
    HttpClientModule ,
    AngularFireModule.initializeApp(firebaseConfig.firebase),
    AngularFireDatabaseModule,
    IonicPageModule.forChild(LoginPage) ,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    OrderPage,
    MyApp,
    HomePage,
    TrackPage,
    AddressBookPage,
    HelpPage,
    EditAddressBookPage,
    AddAddressBookPage,
    TrackingModalPage
  ],
  providers: [
    // Contacts,
    // Contact,
    // ContactName,
    Firebase,
    AuthService,
    ApiService,
    StatusBar,
    SplashScreen,
     {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
