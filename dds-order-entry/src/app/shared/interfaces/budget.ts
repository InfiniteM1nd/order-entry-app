export interface Budget {
  Customer_No: string;
  Id: number;
  Budget: string;
  BudgetName: string;
  Department: string;
  CustomerRef: string;
  WorkOrder: string;
}
