export interface ServiceType {
  Id: string;
  Type: string;
  Mask: string;
}
