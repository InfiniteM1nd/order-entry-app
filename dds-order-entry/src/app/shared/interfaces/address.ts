export interface Address {
  Customer_No: string;
  Id: number;
  Company: string;
  Street: string;
  Floor: string;
  City: string;
  State: string;
  Zipcode: string;
  Contact: string;
  Phone: string;
  Extension?: any;
  Instruction: string;
}
