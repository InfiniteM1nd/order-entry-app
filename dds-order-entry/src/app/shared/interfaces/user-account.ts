export interface UserAccount {
  UserId: number;
  Customer_No: string;
  UserName: string;
  UserPassword: string;
  UserFullName: string;
  UserPhone: string;
  UserExtension: string;
  UserEmail: string;
  UserAccessLevel: string;
  UserActive: number;
  Department: string;
  Budget: string;
  CustomerRef: string;
  WorkOrder: string;
  Company: string;
  Street: string;
  Floor: string;
  City: string;
  State: string;
  Zipcode: string;
  Instruction: string;
  DefaultPickUp: number;
  Authenticated: boolean;
}
