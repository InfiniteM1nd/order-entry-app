export interface Department {
  Customer_No: string;
  Id: number;
  Department: string;
  DepartmentName: string;
  Budget: string;
  CustomerRef: string;
  WorkOrder: string;
  Company: string;
  Street: string;
  Floor: string;
  City: string;
  State: string;
  Zipcode: string;
  Contact: string;
  Phone: string;
  Extension?: any;
  Instruction: string;
  DefaultPickUp: number;
}
