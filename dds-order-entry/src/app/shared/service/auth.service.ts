import { Injectable, OnInit } from '@angular/core';

import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { HttpErrorResponse } from '@angular/common/http/src/response';
import { catchError} from 'rxjs/operators';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { UserAccount } from '../interfaces/user-account';

@Injectable()
export class AuthService implements OnInit {

 private url = "http://doe.deluxedelivery.com/api";

  constructor(private http: HttpClient){}

//login user
  userLogin(credentials): Observable<UserAccount>{
   return  this.http.get<UserAccount>(`${this.url}/${'useraccount'}/${credentials.customerNo}:${credentials.password}`)
   .pipe(
    catchError(this.handleError)
     );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(
      'Something bad happened; please try again later.');
  };

  ngOnInit(){

  }
}
