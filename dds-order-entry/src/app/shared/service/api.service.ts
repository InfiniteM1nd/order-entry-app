import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { catchError} from 'rxjs/operators';
import { ErrorObservable } from 'rxjs/observable/ErrorObservable';
import { Job } from '../interfaces/job';
import { Address } from '../interfaces/address';
import { JobTicket } from '../interfaces/ticket';

import{ Response } from '@angular/http'
import 'rxjs/add/operator/map'
import { ServiceType } from '../interfaces/serviceType';

@Injectable()
export class ApiService {

  private url = "http://doe.deluxedelivery.com/api";

  constructor(private http: HttpClient){}

  saveOrder(orderData): Observable<Job>{
    console.log(orderData)
    return this.http.post<Job>(`${this.url}/${'AddJob'}`, orderData)
    .pipe(
      catchError(this.handleError)
     )
  }

  saveAddress(addressData): Observable<Address>{
   return this.http.post<Address>(`${this.url}/${'address'}`, addressData)
   .pipe(
    catchError(this.handleError)
   )
  }

  getAddressBook(customerNo): Observable<Address[]>{
    return this.http.get<Address[]>(`${this.url}/${'address'}/${customerNo}`)
    .pipe(
      catchError(this.handleError)
     )
  }

  getUserccount(customerNo): Observable<Account>{
    return this.http.get<Account>(`${this.url}/${'UserAccount'}/${customerNo}`)
    .pipe(
      catchError(this.handleError)
     )
  }

  getServiceType(customerNo): Observable<ServiceType[]>{
    return this.http.get<ServiceType[]>(`${this.url}/${'serviceType'}/${customerNo}`)
    .pipe(
      catchError(this.handleError)
    )
  }

  updateAddress(customerNo): Observable<Address>{
    return this.http.post<Address>(`${this.url}/${'address'}/`,customerNo)
    .pipe(
      catchError(this.handleError)
     )
  }

  getTicket(ticketNo): Observable<JobTicket>{
    return this.http.get<JobTicket>(`${this.url}/${'trackJob'}/${ticketNo}`)
    .pipe(
      catchError(this.handleError)
    )
  }
  searchJobByDate(date): Observable<JobTicket[]>{
    return this.http.post<JobTicket[]>(`${this.url}/${'trackJob'}`, date)
    .pipe(
      catchError(this.handleError)
    )
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an ErrorObservable with a user-facing error message
    return new ErrorObservable(
      'Something bad happened; please try again later.');
  };
}
