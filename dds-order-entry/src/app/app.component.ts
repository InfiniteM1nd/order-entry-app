import { Component, ViewChild, OnInit } from '@angular/core';
import { Nav, Platform} from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { TrackPage } from '../pages/tracking/track';
import { OrderPage } from '../pages/order/order';
import { AddressBookPage } from '../pages/addressBook/address.book';
import { LoginPage } from '../pages/login/login';
import { NavController } from 'ionic-angular/navigation/nav-controller';

import { UserAccount } from './shared/interfaces/user-account';

@Component({
  templateUrl: 'app.html'
})
export class MyApp implements OnInit{
  @ViewChild(Nav) nav: NavController;

  rootPage: string = 'LoginPage';
  customerNo: UserAccount;
  pages: Array<any>;

  constructor(public platform: Platform,
     public statusBar: StatusBar,
     public splashScreen: SplashScreen
          ) {

    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      {"title" : "Home", "theme"  : "listViews",  "icon" : " icon-star-circle", "listView" : true, component: HomePage},
      { "title" : "Tracking", "theme"  : "listViews",  "icon" : "ion-ios-eye", "listView" : true, component: TrackPage },
      { "title" : "Place Order", "theme"  : "listViews",  "icon" : " ion-ios-cart-outline", "listView" : true, component: OrderPage },
      { "title" : "Manage Address", "theme"  : "listViews",  "icon" : " ion-ios-settings-outline", "listView" : true, component: AddressBookPage },

    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // this.statusBar.styleDefault();
      this.splashScreen.show();
    });
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
  }
  logout(){
    this.nav.setRoot(LoginPage);
    console.log("logout")
  }

ngOnInit(){

}
}
