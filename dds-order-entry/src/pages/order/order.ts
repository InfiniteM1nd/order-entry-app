import { Component, ViewChild, OnInit } from '@angular/core';
import { Nav, Platform, MenuController, ToastController, ToastOptions } from 'ionic-angular';

import {FormGroup, FormBuilder, Validators, FormControl} from '@angular/forms';
import { ApiService } from '../../app/shared/service/api.service';
import { Job } from '../../app/shared/interfaces/job';
import { Address } from '../../app/shared/interfaces/address';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { UserAccount } from '../../app/shared/interfaces/user-account';
import { ServiceType } from '../../app/shared/interfaces/serviceType';



@Component({
  selector: 'page-order',
  templateUrl: 'order-page.html'
})

export class OrderPage implements OnInit {


  orderForm: FormGroup;
  orderData: Job
  addressBook: Address[];
  userAccount: UserAccount;
  selectedAddress: Address;

  pickupcontact;
  pickupcompany;
  pickupstreet;
  pickupfloor;
  pickupstate;
  pickupcity;
  pickupzip;
  pickupphone;
  pickupinstruction;
  pickupextension;

  deliverycontact;
  deliverycompany;
  deliverystreet;
  deliveryfloor;
  deliverystate;
  deliverycity;
  deliveryzip;
  deliveryphone;
  deliveryinstruction;
  deliveryextension;

  searchControl: FormControl;
  isPickupSearchInactive: boolean = false;
  isDeliverySearchInactive: boolean = false;

  searchTerm: string = '';
  minSelectableDate: string = '';
  pickupDate: string;
  pickupTime: string;
  deliveryDate: string;
  jobtype: string = '';
  servicetype: string = '';

  jobDescription: string[];
  rushJobOptions: string[];
  jobTypes: string[];
  serviceTypes: ServiceType[];
  searchResults: Address[];

  date: Date;

  toastOptions = {
    message: 'Order Successfully Placed:',
    showCloseButton: true,
    closeButtonText: 'OK',
    position: 'bottom'
  }

  constructor(private fb: FormBuilder,
    private apiService: ApiService,
    private toastCtrl: ToastController,
    private navParams: NavParams) {

    this.servicetype = 'Messenger Service';
    this.jobtype = 'Pick-Up';

    this.jobTypes = [
      'Pick-Up',
      'Delivery',
      'Round Trip Pick-Up',
      'Round Trip Delivery'
    ]

    this.jobDescription = ['Envelope', 'Box', 'Miscellaneous'];
    this.rushJobOptions= ['Yes','No'];

    this.pickupDate = new Date().toISOString().slice(0,10);
    this.deliveryDate = new Date().toISOString().slice(0,10);
    this.minSelectableDate = new Date().toISOString().slice(0,10);

    this.searchControl = new FormControl();
    this.userAccount = this.navParams.get('userAccount');
    this.pickupTime = new Date().toLocaleTimeString();

    console.log('Order Component Account Info:', this.userAccount.Company)
    console.log(this.selectedAddress)
    console.log('Date Format:',this.pickupDate)
    console.log('Time Format:',this.pickupTime)
  }

  states = ['Alabama','Alaska','American Samoa','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District of Columbia','Federated States of Micronesia','Florida','Georgia','Guam','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Marshall Islands','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Northern Mariana Islands','Ohio','Oklahoma','Oregon','Palau','Pennsylvania','Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virgin Island','Virginia','Washington','West Virginia','Wisconsin','Wyoming']

  initializeForm(){
    this.orderForm = this.fb.group({
      Ticket_No: '',
      caller_Name: ['',  Validators.required],
      caller_Phone: ['', Validators.required],
      caller_Extension: [''],
      department: ['', Validators.required],
      budget: ['',  Validators.required],
      customerRef: [''],
      workOrder: [''],
      pickup_Company: ['', Validators.required],
      pickup_Street: ['', Validators.required],
      pickup_Floor: [''],
      pickup_City: ['', Validators.required],
      pickup_State: ['', Validators.required],
      pickup_ZipCode: [''],
      pickup_Contact: [''],
      pickup_Phone: [''],
      pickup_Extension: [''],
      pickup_Instruction: [''],
      delivery_Company: ['', ],
      delivery_Street: [''],
      delivery_Floor: [''],
      delivery_City: [''],
      delivery_State: [''],
      delivery_ZipCode: [''],
      delivery_Contact: [''],
      delivery_Phone: [''],
      delivery_Extention: [''],
      delivery_Instruction: [''],

      pickUpDate: ['', Validators.required],
      pickUpTime: ['', Validators.required],

      deliveryByDate: [this.pickupDate,Validators.required],
      deliveryByTime: ['12:00 AM',Validators.required],
      quantity: ['', Validators.required],
      weight:[''],
      description: ['', Validators.required],
      serviceType: ['', Validators.required],
      jobType: ['', Validators.required],
      rush: [''],
      instruction: [''],
      // signReq: [''],
      operator: [''],
      // miles: [''],
      // status: [''],
      // statusCode: [''],
      // statusDate: [''],
      // statusTime: [''],
      // remark_Pod:[''],
      notificationEmail: ['']
    })
  }

  formatDate(date){

    date = new Date().toISOString();

    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();

    if(month.length < 2){
       month = '0' + month;
    }
    if(day.length < 2){
      day = '0' + day
    }

     return [year, month, day].join('-');
   }

   //filter to search address book
   setFilteredAddress(){

    this.searchControl.valueChanges.debounceTime(200).subscribe(term => {
      console.log('Search Result Term:',term)
      if(this.searchResults != null){
         this.searchResults = this.searchFilter(this.searchTerm);
         console.log('SEARCH RESULTS',this.searchResults);

    //   if(this.searchResults != null){
    //    Object.keys(this.searchResults).forEach(key=> {
    //       this.pickupcompany = this.searchResults[key].Company
    //       this.pickupstreet = this.searchResults[key].Street
    //       this.pickupfloor = this.searchResults[key].State
    //       this.pickupcity = this.searchResults[key].City
    //       this.pickupstate = this.searchResults[key].State
    //       this.pickupzip = this.searchResults[key].Zipcode
    //       this.pickupphone = this.searchResults[key].Phone
    //       this.pickupextension = this.searchResults[key].Extension
    //       console.log('Company:',this.pickupcompany)
    //    })
    //  }
    //   if(this.searchResults != null){
    //    Object.keys(this.searchResults).forEach(key=> {
    //     this.deliverycompany = this.searchResults[key].Company
    //     this.deliverystreet = this.searchResults[key].Street
    //     this.deliveryfloor = this.searchResults[key].State
    //     this.deliverycity = this.searchResults[key].City
    //     this.deliverystate = this.searchResults[key].State
    //     this.deliveryzip = this.searchResults[key].Zipcode
    //     this.deliveryphone = this.searchResults[key].Phone
    //     this.deliveryextension = this.searchResults[key].Extension
    //     console.log('Company:',this.deliverycompany)
    //    })
    //  }
      }

     })

        console.log('Search Term Results:', this.searchResults);
  }

  logChosen(): void{
    console.log(this.jobtype)
  }


  loadServiceTypes(){
   this.apiService.getServiceType(this.userAccount.Customer_No)
   .subscribe(service => {
      this.serviceTypes = service;
      console.log('Service Type',this.serviceTypes);
   })
  }

  loadAddressBook(){
    this.apiService.getAddressBook(this.userAccount.Customer_No)
    .subscribe(address => {
     this.addressBook =  address;
     this.searchResults = this.addressBook
     console.log('Loaded AddressBook',this.addressBook);
    })
  }
  autoComplete(jobType){

  if(jobType === 'Pick-Up' || jobType === 'Round Trip Pick-Up'){

    this.isPickupSearchInactive = true;
    this.isDeliverySearchInactive = false;
    console.log('search State',this.isPickupSearchInactive);

    this.deliverycity = '';
    this.deliverycompany = ''
    this.deliverystreet = ''
    this.deliveryfloor =  ''
    this.deliverycity =  ''
    this.deliverystate =  ''
    this.deliveryzip =  ''
    this.deliveryphone =  ''
    this.deliveryinstruction =  ''
    this.deliveryextension =  ''

    this.pickupcompany = this.userAccount.Company
    this.pickupstreet = this.userAccount.Street
    this.pickupfloor =  this.userAccount.Floor
    this.pickupcity =  this.userAccount.City
    // this.pickupstate =  this.userAccount.State

    let pState = this.userAccount.State

    Object.keys(pState).forEach(key=> {
      let matches = pState[key].match(/\b(\w)/g);
      this.pickupstate  = matches.join('');
    })
    this.pickupzip =  this.userAccount.Zipcode
    this.pickupphone =  this.userAccount.UserPhone
    this.pickupinstruction =  this.userAccount.Instruction
    this.pickupextension =  this.userAccount.UserExtension

  }

  if(jobType === 'Delivery' || jobType === 'Round Trip Delivery'){

    this.isDeliverySearchInactive = true;
    this.isPickupSearchInactive = false;
    console.log('search State',this.isDeliverySearchInactive);



    this.pickupcompany = ''
    this.pickupcontact = ''
    this.pickupstreet = ''
    this.pickupfloor =  ''
    this.pickupcity =  ''
    this.pickupstate =  ''
    this.pickupzip =  ''
    this.pickupphone =  ''
    this.pickupinstruction =  ''
    this.pickupextension =  ''

    this.deliverycompany = this.userAccount.Company
    this.deliverystreet = this.userAccount.Street
    this.deliveryfloor =  this.userAccount.Floor
    this.deliverycity =  this.userAccount.City
    this.deliverystate =  this.userAccount.State
    this.deliveryzip =  this.userAccount.Zipcode
    this.deliveryphone =  this.userAccount.UserPhone
    this.deliveryinstruction =  this.userAccount.Instruction
    this.deliveryextension =  this.userAccount.UserExtension
  }

  }
  searchFilter(searchTerm){
    if(searchTerm != '')
    return  this.addressBook.filter(object => {
        return object.Company.toLowerCase().toString().indexOf(searchTerm) > -1 ||
                object.Street.toLowerCase().toString().indexOf(searchTerm)  > -1  ||
                object.City.toLowerCase().toString().indexOf(searchTerm)  > -1  ||
                object.Phone.toLowerCase().toString().indexOf(searchTerm) > -1
     })
   }

  onSubmit(orderData:Job){

    console.log('FORM DATA',orderData)
    orderData.Customer_No =  this.userAccount.Customer_No;

    this.apiService.saveOrder(orderData).subscribe(data => {
      orderData = data
      this.orderForm.reset();
      this.confirmationNotice();
      console.log('Order Succesfully Saved!:', orderData);
    })
  }


  confirmationNotice(){
     this.toastCtrl.create(this.toastOptions).present();
    }

  ionViewDidLoad(){
    this.loadServiceTypes();
    this.loadAddressBook();
    this.setFilteredAddress();
    this.autoComplete(this.jobtype);
  }

  ionViewDidEnter(){

  }
  ngOnInit(){
    this.initializeForm();
  }

}
