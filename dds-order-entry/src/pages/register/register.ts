import { Component,  OnInit } from '@angular/core';
import {NavController, MenuController } from 'ionic-angular';


import { HelpPage } from '../help/help';

import {FormGroup, FormBuilder, Validators} from '@angular/forms';
// import { AuthService } from '../../app/service/auth.service';


@Component({
  selector: 'page-register',
  templateUrl: 'register.html'
})
export class RegisterPage implements OnInit{

  registerForm: FormGroup;
  credentials = []

  constructor(public navCtrl: NavController, private fb: FormBuilder, public menu: MenuController) {

  }

  initializeForm(){
    this.registerForm = this.fb.group({
      userId: ['', Validators.required],
      password: ['',  Validators.required]
    })
  }

//   register({value, valid}: {value: any, valid: boolean}){
//     this.authService.userRegister(value).subscribe(data => {
      
//     })
//   }

  help(params){
    if(!params){
      params = {}
      this.navCtrl.push(HelpPage)
    }
  }

  ionViewDidEnter(){
    this.menu.swipeEnable(false);
  }
  ngOnInit(){
   this.initializeForm();
  }

}
