import { Component, OnInit,  Pipe, PipeTransform } from '@angular/core';
import { NavController, NavParams, AlertController, ToastController, Slides } from 'ionic-angular';
import { FormGroup, FormBuilder,  FormControl  } from '@angular/forms';
// import { Contacts, Contact, ContactName, ContactAddress } from '@ionic-native/contacts';
import { ApiService } from '../../app/shared/service/api.service';

import { EditAddressBookPage } from './edit-address';
import { AddAddressBookPage } from './add-address';

import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/debounceTime';
import { AngularFireDatabase} from 'angularfire2/database';
import { Address } from '../../app/shared/interfaces/address';


@Component({
  selector: 'page-address-book',
  templateUrl: 'address.book.html',
  styleUrls: ['/src/addressBook/address.scss']
})
export class AddressBookPage implements OnInit {

  formGroup: FormGroup;
  searchControl: FormControl;
  searchTerm: string = '';
  userAccount: Address;
  accountAddress: Address[]
  searching: boolean = false;
  addressBook: Address[];
  searchResults: Address[];
  states = ['Alabama','Alaska','American Samoa','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District of Columbia','Federated States of Micronesia','Florida','Georgia','Guam','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Marshall Islands','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Northern Mariana Islands','Ohio','Oklahoma','Oregon','Palau','Pennsylvania','Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virgin Island','Virginia','Washington','West Virginia','Wisconsin','Wyoming']

  constructor(public navCtrl: NavController,
     private fb: FormBuilder,
     private apiService: ApiService,
     private afDatabase: AngularFireDatabase,
     private navParams: NavParams
    ) {
       this.searchControl = new FormControl();
       this.userAccount = this.navParams.get('userAccount')
      }

  addAddress(){
        this.navCtrl.push(AddAddressBookPage, {
          userAccount: this.userAccount
    })
  }

  editAddress(address){
    this.navCtrl.push(EditAddressBookPage,
      {
      userAccount: address,
      customerNo: this.userAccount.Customer_No
      });
  }

  loadAddress(){
    this.apiService.getAddressBook(this.userAccount.Customer_No)
    .subscribe((data) => {
      this.addressBook = data;
      this.searchResults  = this.addressBook
      console.log('Customer NO:', this.userAccount.Customer_No)
    })
  }

  onSearchInput(){
    this.searching = true;
  }

  filterAddress(searchTerm){
    if(!this.addressBook){
      return [];
    }
    if(!searchTerm){
      return  this.addressBook
    }
    if(searchTerm){
         return  this.addressBook.filter((address) => {
      return address.Company.toLowerCase().indexOf(searchTerm) > -1 ||
             address.Phone.toString().indexOf(searchTerm) > -1 ||
             address.Street.toLowerCase().indexOf(searchTerm) > -1 ||
             address.City.toLowerCase().indexOf(searchTerm) > -1;
    })
    }
  }


  setFilteredAddress(){
    this.searchControl.valueChanges.debounceTime(500).subscribe(search => {
        this.searching = false
        this.searchResults = this.addressBook
        this.searchResults =  this.filterAddress(this.searchTerm);
        console.log('Search COntrol:', search);
    })

  }
  // findContact(){
  //   // let contact: Contact = this.contacts.create();
  //   // contact.name = new ContactName(null, 'John','Doe');
  //   // contact.phoneNumbers = [new ContactField('mobile', '9173300123')];
  //   // contact.addresses = [new ContactAddress(null, '68 west 45 St')]
  //   // contact.save().then(
  //   //   () => console.log('Contact saved!', contact),
  //   //   (error: any) => console.error('Error saving contact.', error)
  //   // );
  // }
//   setFilteredItems() {
//   if(this.searchTerm){
//     this.items = this.filterAddress(this.searchTerm);
//   } if(this.searchTerm == null) {
//     return  this.items
//   }


// }


  ionViewDidLoad() {
    this.loadAddress();
    this.setFilteredAddress();
}
  ngOnInit(){

  }
}
