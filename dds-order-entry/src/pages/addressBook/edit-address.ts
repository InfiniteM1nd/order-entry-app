import { Component, OnInit } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { FormGroup, FormBuilder } from '@angular/forms';
import { AngularFireDatabase} from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
// import { Contacts, Contact, ContactName, ContactAddress } from '@ionic-native/contacts';
import { ApiService } from '../../app/shared/service/api.service';
import { ToastController } from 'ionic-angular/components/toast/toast-controller';
import { Address } from '../../app/shared/interfaces/address';
import { AddressBookPage } from './address.book';
import { UserAccount } from '../../app/shared/interfaces/user-account';
@Component({
  selector: 'page-edit-address',
  templateUrl: 'edit-address.html'
})
export class EditAddressBookPage implements OnInit{

  editForm: FormGroup;
  selectedItem: any;
  editableAddress: Address;
  customerNo: Address;
  displayDetailsOnly = true;
  address: Address;

  toastOptions = {
    message: 'Saved!',
    duration: 3000,
    position: 'center'
  }

  states = ['Alabama','Alaska','American Samoa','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District of Columbia','Federated States of Micronesia','Florida','Georgia','Guam','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Marshall Islands','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Northern Mariana Islands','Ohio','Oklahoma','Oregon','Palau','Pennsylvania','Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virgin Island','Virginia','Washington','West Virginia','Wisconsin','Wyoming']

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      private fb: FormBuilder,
      private apiService: ApiService,
      public alertCtrl: AlertController,
      private toastCtrl: ToastController)
         {
            this.editableAddress = this.navParams.get('userAccount');

          }


   initializeAddressBook(){
      this.editForm = this.fb.group({
        id: [this.editableAddress.Id],
        company: [this.editableAddress.Company],
        street: [this.editableAddress.Street],
        floor: [this.editableAddress.Floor],
        city: [this.editableAddress.City],
        state: [this.editableAddress.State],
        zip: [this.editableAddress.Zipcode],
        phone: [this.editableAddress.Phone],
        ext: [this.editableAddress.Extension],
        instructions: [this.editableAddress.Instruction]
      })
  }

  updateAddress({value, valid}: {value: Address, valid: boolean}){
   value.Id =  this.editableAddress.Id;
   this.apiService.updateAddress(value)
    .subscribe((addressData => {

          this.address = addressData;
          this.confirmationNotice();
          console.log(' Address Successfully Updated!')
          console.log(JSON.stringify(addressData));
   }))
  }

  confirmationNotice(){
    let toast = this.toastCtrl.create(this.toastOptions).present();
    }

  editNotice() {
    let toast = this.toastCtrl.create({
      message: 'Type To Edit Address!',
      duration: 3000,
      position: 'top'
    })

    toast.onDidDismiss(() => {
      console.log('Dismissed Edit Notice');
    });

    toast.present();
  }

  editEnabled(){
   this.displayDetailsOnly = false;
   this.editNotice();
  }

  ngOnInit(){

    this.initializeAddressBook();
  }

}
