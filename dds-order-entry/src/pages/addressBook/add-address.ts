import { Component, OnInit } from '@angular/core';
import { NavController,  NavParams, ToastController, AlertController  } from 'ionic-angular';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ApiService } from '../../app/shared/service/api.service';
// import { Contacts, Contact, ContactName, ContactAddress } from '@ionic-native/contacts';
import { Address } from '../../app/shared/interfaces/address';



@Component({
  selector: 'page-add-address',
  templateUrl: 'add-address.html'
})
export class AddAddressBookPage implements OnInit{

  addressForm: FormGroup;

  addressBook: Address;
  userAccount: Address;

  //toast message options
  toastOptions = {
    message: 'Address Saved!',
    duration: 3000,
    position: 'center'
  }

  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      private fb: FormBuilder,
      private apiService: ApiService,
      private toastCtrl: ToastController,
      public alertCtrl: AlertController
    ) {
     this.userAccount =  this.navParams.get('userAccount');
     console.log(this.userAccount.Customer_No)
    }


states= ['Alabama','Alaska','American Samoa','Arizona','Arkansas','California','Colorado','Connecticut','Delaware','District of Columbia','Federated States of Micronesia','Florida','Georgia','Guam','Hawaii','Idaho','Illinois','Indiana','Iowa','Kansas','Kentucky','Louisiana','Maine','Marshall Islands','Maryland','Massachusetts','Michigan','Minnesota','Mississippi','Missouri','Montana','Nebraska','Nevada','New Hampshire','New Jersey','New Mexico','New York','North Carolina','North Dakota','Northern Mariana Islands','Ohio','Oklahoma','Oregon','Palau','Pennsylvania','Puerto Rico','Rhode Island','South Carolina','South Dakota','Tennessee','Texas','Utah','Vermont','Virgin Island','Virginia','Washington','West Virginia','Wisconsin','Wyoming']


initializeAddressBook(){
      this.addressForm = this.fb.group({
        company: ['York College, CUNY123'],
        street: ['69-20 Guy R. Brewer Blvd'],
        floor: ['1rd'],
        city: ['Queens'],
        state: ['New York'],
        zip: ['11451'],
        contact: ['Holly Skir'],
        phone: ['(718) 262-2018'],
        ext: ['2321'],
        instructions: ['test'],
      })
  }

//api service method makes a call to saveAddress,passing the values of the form to add a new address
  addAddress({value, valid}: {value: Address, valid: boolean}) {
    value.Customer_No  = this.userAccount.Customer_No;
    this.apiService.saveAddress(value)
      .subscribe(address => {
        value = address;
        this.addressForm.reset();
        this.confirmationNotice();
      })
  }

  // onSubmit({value, valid}: {value: any, valid: boolean}){
  //   const newAddress = this.addressBook.push({});
  //   newAddress.set({
  //     id: newAddress.key,
  //     company: value.company,
  //     street: value.street,
  //     floor: value.floor,
  //     city: value.city,
  //     state: value.state,
  //     zip: value.zip,
  //     phone: value.phone,
  //     contact: value.contact,
  //     ext: value.ext,
  //     instructions: value.instructions,
  //     email: value.email
  //   })
  //   this.presentToast();
  // }
  // onSubmit({value, valid}: {value: any, valid: boolean}){
  //   this.apiService.saveAddress(value).subscribe(data => {
  //     this.addressBook = data;
  //       this.presentToast();
  //     console.log('Address Succesfully Saved!:', JSON.stringify(this.addressBook));
  //   })
  // }


  // findContact(){
  //   // let contact: Contact = this.contacts.create();
  //   // contact.name = new ContactName(null, 'John','Doe');
  //   // contact.phoneNumbers = [new ContactField('mobile', '9173300123')];
  //   // contact.addresses = [new ContactAddress(null, '68 west 45 St')]
  //   // contact.save().then(
  //   //   () => console.log('Contact saved!', contact),
  //   //   (error: any) => console.error('Error saving contact.', error)
  //   // );
  // }

  //show a toast confirmation message after adding a new address
  confirmationNotice(){
    this.toastCtrl.create(this.toastOptions).present();
  }
//navigate back to home page after the user click done.
  backToHome(){
    this.navCtrl.pop();
  }

  ngOnInit(){
    this.initializeAddressBook();
  }

}
