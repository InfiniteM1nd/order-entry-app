import { Component,  OnInit } from '@angular/core';
import { ViewController } from "ionic-angular/navigation/view-controller";
import { NavParams } from "ionic-angular/navigation/nav-params";
import { Platform } from "ionic-angular/platform/platform";
import { JobTicket } from '../../app/shared/interfaces/ticket';

@Component({
  selector: 'page-track-modal',
  templateUrl: 'tracking-details-modal.html'
})
export class TrackingModalPage {
  searchResults: JobTicket;
  capitalize = 'capitalize';
  constructor(
    public platform: Platform,
    public params: NavParams,
    public viewCtrl: ViewController
  ) {
    this.searchResults = this.params.get('trackingDetails')

  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
