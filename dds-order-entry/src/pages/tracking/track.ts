import { Component,  OnInit } from '@angular/core';
import { NavController,ModalController , NavParams } from 'ionic-angular';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import { ApiService } from '../../app/shared/service/api.service';


import { HttpErrorResponse } from '@angular/common/http/src/response';
import { TrackingModalPage } from './tracking-details-modal';
import { JobTicket } from '../../app/shared/interfaces/ticket';
import { UserAccount } from '../../app/shared/interfaces/user-account';

@Component({
  selector: 'page-track',
  templateUrl: 'track.html'
})

export class TrackPage implements OnInit{
  isActiveTicketNo: boolean = false;
  isActiveDate: boolean = false;

  userAccount: UserAccount;
  outstandingTickets: JobTicket;
  searchResults: JobTicket;
  Results: JobTicket[];
  searchTicketControl: FormControl;
  searchDateControl: FormControl;
  searchTerm: string = '';
  searching: boolean = false;
  startDate: string;
  endDate: string;
  minSelectableYear: string;
  customerNo: string = '';
  date: number;



  constructor(public navCtrl: NavController,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    private apiService: ApiService) {
    // If we navigated to this page, we will have an item available as a nav param
    this.userAccount = navParams.get('userAccount');
    this.searchTicketControl = new FormControl();
    this.searchDateControl = new FormControl();


    this.startDate = new Date().toISOString().slice(0,10);
    this.endDate = new Date().toISOString().slice(0,10);

    this.date =  new Date().getFullYear();
    this.minSelectableYear = this.date.toString()
    console.log('USER ID', this.userAccount.UserId)
    console.log('YEAR', this.minSelectableYear)
    console.log('Current User Data:',this.userAccount.UserId);
 }


getJobByTickets() {
  this.apiService.getTicket(this.searchTerm)
   .subscribe( jobticket => {
     this.searching = false;
     this.searchResults = jobticket;
     console.log('Job Ticket NO:',  this.searchResults.Ticket_No);
   },
   (errorRes: HttpErrorResponse) => {
       if(errorRes.error instanceof Error){
         console.log("Server-Side error occured ")
       }else{
         console.log('Client-Side error occured')
       }
   }
  )
}

getJobByDate(){
  const dateRange = {
    UserId: this.userAccount.UserId,
    FromDate: this.startDate,
    ToDate: this.endDate
  }

  this.apiService.searchJobByDate(dateRange)
  .subscribe(date => {
    this.searching = false;
    this.Results = date
    console.log('DATE RESPONSE',JSON.stringify(this.Results))
  })
}

  selectedFilter(value){
    if(value ==='ticketNo'){
      this.isActiveTicketNo = true;
      this.isActiveDate = false;

      this.startDate = new Date().toISOString();
      this.searchTicketControl.valueChanges.debounceTime(700).subscribe(search => {
        this.searching = false;
        this.getJobByTickets();
        console.log('Searching by TicketNo:',this.searchResults);
    });
    }
    if(value === 'date'){
      this.isActiveDate = true;
      this.isActiveTicketNo = false;
      this.searchTerm = '';

      this.startDate = new Date().toISOString();

     }
  }

  onSearchInput(){
    this.searching = true;
    console.log('MY SEacrh term', this.searchTerm);
  }

  // selectedDateRange(startDate, endDate){

  //   if(startDate > endDate){
  //      return false;
  //   }
  //   while(startDate < endDate){
  //      if(this.searchResults.indexOf(startDate.getDay()) != -1){
  //           this.searchResults.push(new Date(startDate))
  //      }
  //    startDate.setDate(startDate.getDay() + 1)
  //   }
  //   return this.searchResults;
  // }

  // filterSearch(searchTerm){

  //   if(searchTerm === ''){
  //     return this.items;
  //   }else{
  //     return  this.items.filter((item) => {
  //       if(this.isActiveTicketNo === true){
  //           return  item.Status.toLowerCase().includes(searchTerm);
  //       }
  //       // if(this.isActiveDate === true) {

  //       //     // return  item.DeliveryByDate.toLowerCase().includes(searchTerm);
  //       // }
  //     })
  //   }
  // }

  presentModal() {
    let modal = this.modalCtrl.create(TrackingModalPage,{
      trackingDetails: this.searchResults
    });
    modal.present();
  }

  ionViewDidLoad() {
    this.getJobByDate();
    this.searching = false;
    this.searchDateControl.valueChanges.debounceTime(700).subscribe(search => {
      this.searching = false;
       this.getJobByDate();
    });

    this.isActiveDate = true;
    this.isActiveTicketNo = false;
}

ngOnInit(){

}
}
