import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, NavController, NavParams } from 'ionic-angular';

import { TrackPage } from '../tracking/track';
import { OrderPage } from '../order/order';
import { AddressBookPage } from '../addressBook/address.book';
import { UserAccount } from '../../app/shared/interfaces/user-account';
import { HelpPage } from '../help/help';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public userAccount:UserAccount;

  constructor(public navCtrl: NavController, private navParams: NavParams) {
       this.userAccount = this.navParams.get('userAccount');
       console.log('USER ID', this.userAccount.UserId)
       console.log('Home Component Account Info', this.userAccount.Company)


  }

  openOrderPage(params){
    if(!params) {
      params = {};
      this.navCtrl.push(OrderPage, {
        userAccount: this.userAccount
      });
    }
  }
  openHelpPage(params){
    if(!params){
      params = {}
      this.navCtrl.push(HelpPage)
   }
  }
  openAddressBookPage(params){
    if(!params){
      params = {}
      this.navCtrl.push(AddressBookPage, {
        userAccount: this.userAccount
      })
   }
  }
  openTrackingPage(params){
    if(!params){
      params = {}
      this.navCtrl.push(TrackPage, {
        userAccount: this.userAccount
      })
    }
  }
}
