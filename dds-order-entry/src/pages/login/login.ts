import { Component,  OnInit } from '@angular/core';
import {NavController, MenuController, IonicPage } from 'ionic-angular';

import {FormGroup, FormBuilder} from '@angular/forms';

import { HomePage } from '../home/home';
import { HelpPage } from '../help/help';


import { AuthService } from '../../app/shared/service/auth.service';
import { UserAccount } from '../../app/shared/interfaces/user-account';



@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage implements OnInit{

  loginForm: FormGroup;
  credentials: UserAccount;  //login credentials object of type UserAccount

  constructor(
    public navCtrl: NavController,
    private fb: FormBuilder,
    private authService: AuthService,
    public menu: MenuController) {}

    //loginForm form group object
  initializeForm(){
    this.loginForm = this.fb.group({
      customerNo: ['PRICE'],
      password: ['price']
    })
  }

  // call auth Service to get user
  login({value, valid}: {value: any, valid: boolean}){
    this.authService.userLogin(value).subscribe(accountInfo => {
      this.credentials = accountInfo;

      console.log('DATA:', accountInfo.Company)

      console.log('Accepting credentials' + value.customerNo + '-' + value.password);
      console.log('Logged Credentials Correct:', this.credentials.Customer_No + '-' + this.credentials.UserPassword);
      console.log('AUthentication Status:', this.credentials.Authenticated);
      console.log('Account No', this.credentials.Customer_No)
      if(this.credentials.Authenticated === true){
              this.navCtrl.setRoot(HomePage, {userAccount: accountInfo});
        console.log('login successful');
      }else {
        console.log('Failed to login!: Check user Id or password');
      }
    })
  }

  //navigate to help page
  help(params){
    if(!params){
      params = {}
      this.navCtrl.push(HelpPage)
    }
  }

  //disable swipe menu on login page
  ionViewDidEnter(){
    this.menu.swipeEnable(false);
  }

  //initialize login form
  ngOnInit(){
   this.initializeForm();
  }

}
