import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, NavController } from 'ionic-angular';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { UserAccount } from '../../app/shared/interfaces/user-account';


@Component({
  selector: 'page-help',
  templateUrl: 'help.html'
})
export class HelpPage {

  @ViewChild(Nav) nav: Nav;

  userAccount: UserAccount;
  items = ['Forgot your password?', 'Placing an order?', 'Tracking an order?']
  constructor(public navCtrl: NavController, private navParams: NavParams) {
    this.userAccount = this.navParams.get('userAccount');
  }

  itemSelected(){
    console.log('HELP DETAILS')
  }

}
