import { Injectable,  Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the SearchPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'search',
  pure: true
})
@Injectable()
export class SearchPipe {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(items: any[], terms: string): any[] {
    // if(!items) return [];
    // if(!terms) return items;
  if(terms){
        terms = terms.toLowerCase();
    console.log('Terms:' + terms)
    return items.filter( value => {
      console.log(value.company)
      return value.company.toLowerCase().indexOf(terms) > -1;
    })
  }else {
    return items;
  }


  }
}
// || 
// value.zip.toLowerCase().includes(terms) || 
// value.street.toLowerCase().includes(terms)