import { Injectable,  Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the SearchPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'objectVal',
  pure: true
})
@Injectable()
export class DisplayObjectValuePipe implements PipeTransform {

    transform(value: any, args?: any[]): any[] {
        // create instance vars to store keys and final output
        let keyArr: any[] = Object.keys(value),
            dataArr = [];

        // loop through the object,
        // pushing values to the return array
        keyArr.forEach((key: any) => {
            dataArr.push(value[key]);
            console.log(`key is ${key} and value is ${value[key]}`);
        });

        // return the resulting array
        return dataArr;
    }

}
