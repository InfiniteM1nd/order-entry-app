const pg = require('pg');
const connectionString = process.env.DATABASE_URL || 'postgres://localhost:5432/server';

const client = new pg.Client(connectionString);
client.connect();


const query = client.query('CREATE TABLE ADDRESS(id SERIAL PRIMARY KEY, company not null, street not null, floor not null, city not null, zip not null, contact not null, phone not null, ext not null, instructions not null, email not null )');
